require('dotenv').config();
require('@tensorflow/tfjs-node');

const Promise = require('bluebird');
const NodeWebcam = require('node-webcam');
const canvas = require('canvas');
const faceapi = require('face-api.js');
const path = require('path');
const fetch = require('node-fetch');

const logger = require('./logger');

const { Canvas, Image, ImageData } = canvas;
faceapi.env.monkeyPatch({ Canvas, Image, ImageData, fetch });

const webcamOptions = {
  width: 1280,
  height: 720,
  quality: 100,
  saveShots: true,
  output: 'jpeg',
  device: false,
  callbackReturn: 'location',
  verbose: true,
};

const faceModelsUri = path.join(__dirname, '/models');
const faceDetectorOptions = new faceapi.TinyFaceDetectorOptions({
  // Size at which image is processed, the smaller the faster, but less precise in detecting smaller faces, must be divisible by 32, common sizes are 128, 160, 224, 320, 416, 512, 608, for face tracking via webcam I would recommend using smaller sizes, e.g. 128, 160, for detecting smaller faces use larger sizes, e.g. 512, 608
  inputSize: 128,

  // Minimum confidence threshold
  scoreThreshold: 0.5,
});

async function initFacesApi() {
  await faceapi.nets.tinyFaceDetector.loadFromDisk(faceModelsUri);
  await faceapi.nets.faceLandmark68Net.loadFromDisk(faceModelsUri);
  await faceapi.nets.faceRecognitionNet.loadFromDisk(faceModelsUri);
}

async function detectSingleFaceFromStandardImage(image) {
  const imageData = await canvas.loadImage(image);

  const result = await faceapi
    .detectSingleFace(imageData, faceDetectorOptions)
    .withFaceLandmarks()
    .withFaceDescriptor();

  logger.debug(result.detection);

  return {
    result,
    faceMatcher: result ? new faceapi.FaceMatcher(result) : null,
  };
}

async function detectAllFacesFromStandardImage(image) {
  const imageData = await canvas.loadImage(image);

  const results = await faceapi
    .detectAllFaces(imageData, faceDetectorOptions)
    .withFaceLandmarks()
    .withFaceDescriptors();

  logger.debug(results);

  return {
    results,
    faceMatcher: results.length > 0 ? new faceapi.FaceMatcher(results) : null,
  };
}

function buildLabeledFaceDescritorFromDescriptor(name, descriptors) {
  return new faceapi.LabeledFaceDescriptors(name, descriptors);
}

// async function buildLabeledFaceDescritorFromAlignedImageUris(name, imageUris) {
//   return buildLabeledFaceDescritorFromDescriptor(
//     name,
//     await Promise.map(imageUris, async imageUri => {
//       const faceInput = await canvas.loadImage(imageUri);
//       return faceapi.computeFaceDescriptor(faceInput);
//     }),
//   );
// }

async function buildLabeledFaceDescritorFromStandardImage(name, images) {
  logger.debug(`Build face descritor for ${name}`);

  return buildLabeledFaceDescritorFromDescriptor(
    name,
    await Promise.map(images, async image => {
      const singleFace = await detectSingleFaceFromStandardImage(image);
      return singleFace.result.descriptor;
    }),
  );
}

function buildFaceMatcherFromDescritor(descriptors) {
  return descriptors.length > 0 ? new faceapi.FaceMatcher(descriptors) : null;
}

async function matchWithFaceFromStandardImage(image, faceMatcher) {
  const allFaces = await detectAllFacesFromStandardImage(image);

  const matchs = [];
  if (allFaces && allFaces.results > 0) {
    allFaces.results.forEach(fd => {
      matchs.push(faceMatcher.findBestMatch(fd.descriptor));
    });
  }

  logger.debug(matchs);

  return {
    results: allFaces,
    matchs,
  };
}

async function job() {
  logger.info('Loading models...');
  await initFacesApi();

  // Compute descriptors
  logger.info('Loading face descriptors...');
  const facesMatcher = buildFaceMatcherFromDescritor([
    await buildLabeledFaceDescritorFromStandardImage('Clémence', [
      './images/descriptors/clemence.jpg',
    ]),
    await buildLabeledFaceDescritorFromStandardImage('Clément', [
      './images/descriptors/clement.jpg',
    ]),
    await buildLabeledFaceDescritorFromStandardImage('Ervin', [
      './images/descriptors/ervin.jpg',
    ]),
    await buildLabeledFaceDescritorFromStandardImage('Florian', [
      './images/descriptors/florian.jpg',
    ]),
  ]);

  // iterate webcam images
  logger.info('Feching webcam images...');

  let webcam;
  try {
    webcam = NodeWebcam.create(webcamOptions);
  } catch (err) {
    logger.err(err);
    return;
  }

  while (true) {
    logger.debug('Capture webcam frame');
    let matches;

    try {
      matches = await new Promise((resolve, reject) => { // eslint-disable-line no-new, no-await-in-loop, prettier/prettier, no-loop-func
        webcam.capture('./tmp/webcam', async (err, image) => {
          if (err) reject(err);
          logger.debug('Frame captured');
          const faceMatch = await matchWithFaceFromStandardImage(
            image,
            facesMatcher,
          );
          resolve(faceMatch);
        });
      });
    } catch (err) {
      logger.err(err);
    }

    if (matches) logger.debug(matches);
  }
}

job();
