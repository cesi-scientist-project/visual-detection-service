const debug = require('debug')('cesi:msia18:visual-detection-service');
const chalk = require('chalk');
const util = require('util');

/**
 * Logger middleware, you can customize it to make messages more personal
 */
const logger = {
  debug: (str, ...attr) => {
    debug(str, ...attr);
  },

  info: (str, ...attr) => {
    console.log(util.format(str, ...attr)); // eslint-disable-line no-console
  },

  warn: (str, ...attr) => {
    console.warn(chalk.yellow(util.format(str, ...attr))); // eslint-disable-line no-console
  },

  err: err => {
    console.error(chalk.red(err.message)); // eslint-disable-line no-console
  },
};

module.exports = logger;
